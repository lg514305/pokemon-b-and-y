import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from random import randint
def send_mes(user_id, message, keyboard = None):
    vk.method('messages.send', {'user_id': user_id, 'message': message, 'random_id' : randint(1,32000), 'keyboard':keyboard})
def send_med(user_id, message, media, keyboard = None):
    vk.method('messages.send', {'user_id': user_id, 'message': message, 'attachment': media, 'random_id' : randint(1,32000), 'keyboard':keyboard})
    
# API-ключ созданный ранее
token = "483aeaf72efa9256be3a5bdebe2ea23949197318b975fd4de208565646355e3e14164e4a32ba5e6b1ab53"

# Авторизуемся как сообщество
vk = vk_api.VkApi(token=token)
# Работа с сообщениями
longpoll = VkLongPoll(vk)
send_mes(362766709, "Server has been started")
chs = 0
st = 0
l2 = 1
game_state = {'started': False, 'mode': 0, 'Choise': 0, 'Battle': 0, 'Pass': 0, 'Turn': 1,
                'Pokemon1': None, 'Pokemon2': None, 'P1Health': 100, 'P2Health': 100, 'Atk1': None, 'Atk2': None,
                'Psn1': 0, 'Psn2': 0, 'P1Stop': 0, 'P2Stop': 0}
pkm = {'Бульбазавр': {'Виноградная плеть': 20, 'Семена-пиявки': 'dr', 'Ядовитая пыль': 'psn', 'Таран': 26, 'Speed': 49, 'Atk': ('Виноградная плеть', 'Семена-пиявки', 'Ядовитая пыль', 'Таран'), 'Name': 'Бульбазавр'},
        'Саблай': {'Царапание': 20, 'Ночная тень': 'xpeq', 'Когти ярости': 10 * randint(1, 3), "Грязная игра": 'atkeq', 'Speed': 52, "Atk": ('Царапание', 'Ночная тень', 'Когти ярости', 'Грязная игра'), 'Name': 'Саблай'},
        "Пилосвайн": {'Яростная атака': 9 * randint(2, 4), 'Пурга': "stop", 'Ледяной ветер': 'sdown', 'Таран': 26, 'Speed': 46, 'Atk': ('Яростная атака', 'Пурга', 'Ледяной ветер', 'Таран'), 'Name': 'Пилосвайн'},
        'Ликилики': {'Облизывание': "stop", 'Мощная плеть': 28, 'Выжимание': 'xpeq', "Подзатыльник": 19, 'Speed': 50, "Atk": ('Облизывание', 'Мощная плеть', 'Выжимание', 'Подзатыльник'), 'Name': 'Ликилики'}}

pkmList = ("Бульбазавр", "Саблай", "Пилосвайн", "Ликилики")

startKey = '''{
    "one_time": true,
    "buttons": [
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": "Start"
            },
            "color": "positive"
        }]
    ]
}'''

choiseKey =  '''{
    "one_time": true,
    "buttons": [
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": "Один на один"
            },
            "color": "positive"
        }, {
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": "С ботом"
            },
            "color": "primary"
        }]
    ]
 }'''

pokemonKey =  '''{
    "one_time": true,
    "buttons": [
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": "Бульбазавр"
            },
            "color": "primary"
        }, {
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": "Саблай"
            },
            "color": "primary"
        }], 
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": "Пилосвайн"
            },
            "color": "primary"
        }, {
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": "Ликилики"
            },
            "color": "primary"
        }]
    ]
 }'''

battleKey = '''{
    "one_time": true,
    "buttons": [
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": "Битва"
            },
            "color": "negative"
        }]
    ]
}'''

pokemon1Key =  '''{
    "one_time": true,
    "buttons": [
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": pkm[game_state['Pokemon1']]['Atk'][1]
            },
            "color": "primary"
        }, {
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": pkm[game_state['Pokemon1']]['Atk'][2]
            },
            "color": "primary"
        }],
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": pkm[game_state['Pokemon1']]['Atk'][3]
            },
            "color": "primary"
        }, {
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": pkm[game_state['Pokemon1']]['Atk'][4]
            },
            "color": "primary"
        }]
    ]
 }'''

pokemon2Key =  '''{
    "one_time": true,
    "buttons": [
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": pkm[game_state['Pokemon2']]['Atk'][1]
            },
            "color": "primary"
        }, {
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": pkm[game_state['Pokemon2']]['Atk'][2]
            },
            "color": "primary"
        }],
        [{
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"1\\"}",
                "label": pkm[game_state['Pokemon2']]['Atk'][3]
            },
            "color": "primary"
        }, {
            "action": {
                "type": "text",
                "payload": "{\\"button\\": \\"2\\"}",
                "label": pkm[game_state['Pokemon2']]['Atk'][4]
            },
            "color": "primary"
        }]
    ]
 }'''

def processEvent(event):
    global game_state
    global a
    if not game_state['started'] and event.text.lower() in ' start  старт'.split():
        game_state['started'] = True
        send_mes(event.user_id, 'Какой режим вы хотите выбрать?', choiseKey)
    elif game_state['started']:
        if not game_state['mode'] and event.text in ' С ботом  с ботом  бот  bot  with bot':
            game_state['mode'] = 1
            send_mes(event.user_id, 'Вы выбрали режим с ботом')
            send_mes(event.user_id, 'Выберите стартового покемона', pokemonKey)
        elif not game_state['mode'] and event.text in ' Один на один  один на один  1 на 1  one v one  1 v 1  1v1':
            game_state['mode'] = 2
            send_mes(event.user_id, 'Вы выбрали режим один на один')
            send_mes(event.user_id, 'Первый игрок, выберите стартового покемона', pokemonKey)
        elif game_state['mode'] > 0:
            if game_state['Choise'] == 0:
                try:
                    game_state['Pokemon1'] = event.text
                    game_state['Choise'] = 1
                    send_mes(event.user_id, str(pkm[game_state['Pokemon1']]['Name'])+" - неплохой выбор!")
                    if game_state['mode'] == 2:
                        send_mes(event.user_id, 'Второй игрок, выберите стартового покемона', pokemonKey)
                    else:
                        game_state['Pokemon2'] = pkmList[randint(0, 3)]
                        send_mes(event.user_id, "Противник выбирает "+str(pkm[game_state['Pokemon2']]['Name']))
                        send_mes(event.user_id, 'Напишите "Битва" чтобы начать', battleKey)
                        game_state['Choise'] = 2
                except:
                    send_mes(event.user_id, 'Неверное имя покемона, доступные: Бульбазавр, Саблай', pokemonKey)
            elif game_state['Choise'] == 1:
                    try:
                        game_state['Pokemon2'] = event.text
                        send_mes(event.user_id, str(pkm[game_state['Pokemon2']]['Name'])+" - неплохой выбор!")
                        send_mes(event.user_id, 'Напишите "Битва" чтобы начать', battleKey)
                        game_state['Choise'] = 2
                    except:
                        send_mes(event.user_id, 'Неверное имя покемона, доступные: Бульбазавр, Саблай', pokemonKey)
            elif game_state['Choise'] == 2:
                if 'Битва' in event.text:
                    send_mes(event.user_id, 'Битва началась!')
                    if pkm[game_state['Pokemon1']]['Speed'] > pkm[game_state['Pokemon2']]['Speed']:
                        game_state['Turn'] = 1
                        send_mes(event.user_id, '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
                    elif pkm[game_state['Pokemon1']]['Speed'] < pkm[game_state['Pokemon2']]['Speed']:
                        game_state['Turn'] = 2
                        if game_state['mode'] == 2:
                            send_mes(event.user_id, '2-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon2']]['Atk']))
                        else:
                            game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
                            game_state['Pass'] = 2
                            PvP(event)
                    elif pkm[game_state['Pokemon1']]['Speed'] == pkm[game_state['Pokemon2']]['Speed']:
                        game_state['Turn'] = 1
                        send_mes(event.user_id, '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
                    game_state['Battle'] = 1
                elif game_state['Battle'] == 1 and game_state['Pass'] == 0:
                    if pkm[game_state['Pokemon1']]['Speed'] > pkm[game_state['Pokemon2']]['Speed']:
                        game_state['Turn'] = 1
                        game_state['Pass'] = 1
                    elif pkm[game_state['Pokemon1']]['Speed'] < pkm[game_state['Pokemon2']]['Speed']:
                        game_state['Turn'] = 2
                        game_state['Pass'] = 2
                    elif pkm[game_state['Pokemon1']]['Speed'] == pkm[game_state['Pokemon2']]['Speed']:
                        game_state['Turn'] = 1
                        game_state['Pass'] = 1
                    PvP(event)
                elif game_state['Pass'] == 1:
                    game_state['Turn'] = 2
                    game_state['Pass'] = 0
                    PvP(event)
                    game_state['Atk2'] = None
                elif game_state['Pass'] == 2:
                    game_state['Turn'] = 1
                    game_state['Pass'] = 0
                    PvP(event)
                    game_state['Atk1'] = None

        elif not game_state['mode']:
            send_mes(event.user_id, 'Выберите режим игры: С ботом или Один на один', choiseKey)
    elif not game_state['started']:
        send_mes(event.user_id, 'Напишите "start", чтобы начать игру', startKey)

def PvP(event):
    global a
    global game_state
    if game_state['Turn'] == 1 and game_state['P1Health'] > 0:
        if event.text in str(pkm[game_state['Pokemon1']]['Atk']):
            game_state['Atk1'] = event.text
        elif event.text not in str(pkm[game_state['Pokemon1']]['Atk']):
            send_mes(event.user_id, '1-й игрок, выбирайте одну из атак: '+str(pkm[game_state['Pokemon1']]['Atk']))
        if game_state['Atk1'] and game_state['P1Stop'] == 0:
            send_mes(event.user_id, game_state['Pokemon1']+' использует '+game_state['Atk1']+'!')
            try:
                game_state['P2Health'] -= pkm[game_state['Pokemon1']][game_state['Atk1']]
                send_mes(event.user_id, 'Наносит '+str(pkm[game_state['Pokemon1']][game_state['Atk1']])+' урона противнику!')
            except:
                if pkm[game_state['Pokemon1']][game_state['Atk1']] == 'psn':
                    game_state['Psn2'] = 4
                    send_mes(event.user_id, game_state['Pokemon1']+' отравляет противника!')
                elif pkm[game_state['Pokemon1']][game_state['Atk1']] == 'dr':
                    send_mes(event.user_id, 'Забирает 10 очков здоровья себе!')
                    game_state['P2Health'] -= 10
                    if game_state['P1Health'] < 91:
                        game_state['P1Health'] += 10
                    else:
                        game_state['P1Health'] = 100
                elif pkm[game_state['Pokemon1']][game_state['Atk1']] == 'xpeq':
                    game_state['P2Health'] = int(game_state['P2Health']/4*3)
                    send_mes(event.user_id, 'Сносит четверть здоровья противника!')
                elif pkm[game_state['Pokemon1']][game_state['Atk1']] == 'stop':
                    game_state['P2Stop'] = 2
                    send_mes(event.user_id, 'Усыпляет/замораживает/контузит/пaрализует противника!')
                elif pkm[game_state['Pokemon1']][game_state['Atk1']] == 'sdown':
                    pkm[game_state['Pokemon2']]['Speed'] -= 6
                    send_mes(event.user_id, "Понижает скорость противника!")
                elif pkm[game_state['Pokemon1']][game_state['Atk1']] == 'atkeq':
                    try:
                        game_state['P2Health'] -= pkm[game_state['Pokemon2']][game_state['Atk2']]
                    except:
                        send_mes(event.user_id, 'Предыдущая атака '+game_state['Pokemon2']+" не несет урона, или еще не была проведена!")
                        send_mes(event.user_id, 'Промах!')
            if game_state['Psn2'] > 0:
                game_state['P2Health'] -= 9
                game_state['Psn2'] -= 1
            if game_state['Psn1'] > 0:
                game_state['P1Health'] -= 9
                game_state['Psn1'] -= 1
            send_mes(event.user_id, 'Здоровье '+game_state['Pokemon2']+" равно "+str(game_state['P2Health']))
            send_mes(event.user_id, 'Здоровье ' + game_state['Pokemon1'] + " равно " + str(game_state['P1Health']))
            game_state['Atk2'] = None
            if game_state['Pass'] == 0:
                if pkm[game_state['Pokemon1']]['Speed'] > pkm[game_state['Pokemon2']]['Speed']:
                    send_mes(event.user_id, '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
                elif pkm[game_state['Pokemon1']]['Speed'] < pkm[game_state['Pokemon2']]['Speed']:
                    if game_state['mode'] == 2:
                        send_mes(event.user_id,
                                '2-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon2']]['Atk']))
                    else:
                        game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
                        game_state['Turn'] = 2
                        game_state['Pass'] = 2
                        PvP(event)
                elif pkm[game_state['Pokemon1']]['Speed'] == pkm[game_state['Pokemon2']]['Speed']:
                    send_mes(event.user_id,
                                '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
            elif game_state['Pass'] > 0:
                if game_state['mode'] == 2:
                    send_mes(event.user_id,
                             '2-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon2']]['Atk']))
                else:
                    game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
                    game_state['Turn'] = 2
                    game_state['Pass'] = 0
                    PvP(event)

        elif game_state['P1Stop'] == 1:
            send_mes(event.user_id, game_state['Pokemon1'] + " спит/парализован/заморожен/окантужен и не может двигаться")
            if game_state['Psn2'] > 0:
                game_state['P2Health'] -= 9
                game_state['Psn2'] -= 1
            if game_state['Psn1'] > 0:
                game_state['P1Health'] -= 9
                game_state['Psn1'] -= 1
            send_mes(event.user_id, 'Здоровье ' + game_state['Pokemon2'] + " равно " + str(game_state['P2Health']))
            send_mes(event.user_id, 'Здоровье ' + game_state['Pokemon1'] + " равно " + str(game_state['P1Health']))
            game_state['Atk2'] = None
            game_state['P1Stop'] -= 1
            if game_state['Pass'] == 0:
                if pkm[game_state['Pokemon1']]['Speed'] > pkm[game_state['Pokemon2']]['Speed']:
                    send_mes(event.user_id,
                             '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
                elif pkm[game_state['Pokemon1']]['Speed'] < pkm[game_state['Pokemon2']]['Speed']:
                    if game_state['mode'] == 2:
                        send_mes(event.user_id,
                                 '2-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon2']]['Atk']))
                    else:
                        game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
                        game_state['Turn'] = 2
                        game_state['Pass'] = 2
                        PvP(event)
                elif pkm[game_state['Pokemon1']]['Speed'] == pkm[game_state['Pokemon2']]['Speed']:
                        send_mes(event.user_id,
                                 '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
            elif game_state['Pass'] > 0:
                if game_state['mode'] == 2:
                    send_mes(event.user_id,
                             '2-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon2']]['Atk']))
                else:
                    game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
                    game_state['Turn'] = 2
                    game_state['Pass'] = 0
                    PvP(event)


    elif game_state['Turn'] == 2 and game_state['P2Health'] > 0:
        if game_state['Atk2'] is None and event.text in str(pkm[game_state['Pokemon2']]['Atk']) and game_state['mode'] == 2:
            game_state['Atk2'] = event.text
        elif not game_state['Atk2'] and event.text not in str(pkm[game_state['Pokemon2']]['Atk']) and game_state['mode'] == 2:
            send_mes(event.user_id, '2-й игрок, выбирайте одну из атак: '+str(pkm[game_state['Pokemon2']]['Atk']))
        elif game_state['mode'] == 1:
            game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
        if game_state['Atk2'] and game_state['P2Stop'] == 0:
            send_mes(event.user_id, game_state['Pokemon2']+' использует '+game_state['Atk2']+'!')
            try:
                game_state['P1Health'] -= pkm[game_state['Pokemon2']][game_state['Atk2']]
                send_mes(event.user_id, 'Наносит '+str(pkm[game_state['Pokemon2']][game_state['Atk2']])+' урона противнику!')
            except:
                if pkm[game_state['Pokemon2']][game_state['Atk2']] == 'psn':
                    game_state['Psn1'] = 4
                    send_mes(event.user_id, game_state['Pokemon2']+' отравляет противника!')
                elif pkm[game_state['Pokemon2']][game_state['Atk2']] == 'dr':
                    send_mes(event.user_id, 'Забирает 10 очков здоровья себе!')
                    game_state['P1Health'] -= 10
                    if game_state['P2Health'] < 91:
                        game_state['P2Health'] += 10
                    else:
                        game_state['P2Health'] = 100
                elif pkm[game_state['Pokemon2']][game_state['Atk2']] == 'xpeq':
                    game_state['P1Health'] = int(game_state['P1Health']/4*3)
                    send_mes(event.user_id, 'Сносит четверть здоровья противника!')
                elif pkm[game_state['Pokemon2']][game_state['Atk2']] == 'stop':
                    game_state['P1Stop'] = 2
                    send_mes(event.user_id, 'Усыпляет/замораживает/контузит/парализует противника!')
                elif pkm[game_state['Pokemon2']][game_state['Atk2']] == 'sdown':
                    pkm[game_state['Pokemon1']]['Speed'] -= 6
                    send_mes(event.user_id, "Понижает скорость противника!")
                elif pkm[game_state['Pokemon2']][game_state['Atk2']] == 'atkeq':
                    try:
                        game_state['P1Health'] -= pkm[game_state['Pokemon1']][game_state['Atk1']]
                    except:
                        send_mes(event.user_id, 'Предыдущая атака '+game_state['Pokemon1']+" не несет урона, или еще не была проведена!")
                        send_mes(event.user_id, 'Промах!')
            if game_state['Psn1'] > 0:
                game_state['P1Health'] -= 9
                game_state['Psn1'] -= 1
            if game_state['Psn2'] > 0:
                game_state['P2Health'] -= 9
                game_state['Psn2'] -= 1
            send_mes(event.user_id, 'Здоровье '+game_state['Pokemon1']+" равно "+str(game_state['P1Health']))
            send_mes(event.user_id, 'Здоровье ' + game_state['Pokemon2'] + " равно " + str(game_state['P2Health']))
            game_state['Atk1'] = None
            if game_state['Pass'] == 0:
                if pkm[game_state['Pokemon1']]['Speed'] > pkm[game_state['Pokemon2']]['Speed']:
                    send_mes(event.user_id,
                             '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
                elif pkm[game_state['Pokemon1']]['Speed'] < pkm[game_state['Pokemon2']]['Speed']:
                    if game_state['mode'] == 2:
                        send_mes(event.user_id,
                                 '2-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon2']]['Atk']))
                    else:
                        game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
                        game_state['Turn'] = 2
                        game_state['Pass'] = 2
                        PvP(event)
                elif pkm[game_state['Pokemon1']]['Speed'] == pkm[game_state['Pokemon2']]['Speed']:
                        send_mes(event.user_id,
                                 '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
            elif game_state['Pass'] > 0:
                send_mes(event.user_id, '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))

        elif game_state['P2Stop'] > 0:
            send_mes(event.user_id, game_state['Pokemon2'] + " парализован/заморожен/окантужен и не может двигаться")
            if game_state['Psn2'] > 0:
                game_state['P2Health'] -= 9
                game_state['Psn2'] -= 1
            if game_state['Psn1'] > 0:
                game_state['P1Health'] -= 9
                game_state['Psn1'] -= 1
            send_mes(event.user_id, 'Здоровье ' + game_state['Pokemon1'] + " равно " + str(game_state['P2Health']))
            send_mes(event.user_id, 'Здоровье ' + game_state['Pokemon2'] + " равно " + str(game_state['P1Health']))
            game_state['Atk1'] = None
            game_state['P2Stop'] -= 1
            if game_state['Pass'] == 0:
                if pkm[game_state['Pokemon1']]['Speed'] > pkm[game_state['Pokemon2']]['Speed']:
                    send_mes(event.user_id,
                             '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
                elif pkm[game_state['Pokemon1']]['Speed'] < pkm[game_state['Pokemon2']]['Speed']:
                    if game_state['mode'] == 2:
                        send_mes(event.user_id,
                                 '2-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon2']]['Atk']))
                    else:
                        game_state['Atk2'] = pkm[game_state['Pokemon2']]['Atk'][randint(0, 3)]
                        game_state['Turn'] = 2
                        game_state['Pass'] = 2
                        PvP(event)
                elif pkm[game_state['Pokemon1']]['Speed'] == pkm[game_state['Pokemon2']]['Speed']:
                    send_mes(event.user_id,
                                        '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))
            elif game_state['Pass'] > 0:
                send_mes(event.user_id, '1-й игрок, выбирайте одну из атак: ' + str(pkm[game_state['Pokemon1']]['Atk']))

    elif game_state['P1Health'] <= 0:
        send_mes(event.user_id, game_state['Pokemon2']+' победил!')
        game_state = {'started': False, 'mode': 0, 'Choise': 0, 'Battle': 0, 'Pass': 0, 'Turn': 1,
                      'Pokemon1': None, 'Pokemon2': None, 'P1Health': 100, 'P2Health': 100, 'Atk1': None, 'Atk2': None,
                      'Psn1': 0, 'Psn2': 0, 'P1Stop': 0, 'P2Stop': 0}
        send_mes(event.user_id, 'Напишите "start", чтобы начать игру', startKey)
    elif game_state['P2Health'] <= 0:
        send_mes(event.user_id, game_state['Pokemon1']+' победил!')
        game_state = {'started': False, 'mode': 0, 'Choise': 0, 'Battle': 0, 'Pass': 0, 'Turn': 1,
                      'Pokemon1': None, 'Pokemon2': None, 'P1Health': 100, 'P2Health': 100, 'Atk1': None, 'Atk2': None,
                      'Psn1': 0, 'Psn2': 0, 'P1Stop': 0, 'P2Stop': 0}
        send_mes(event.user_id, 'Напишите "start", чтобы начать игру', startKey)

print("Server started")
for event in longpoll.listen():
    if event.type == VkEventType.MESSAGE_NEW:
        if event.to_me:
            processEvent(event)